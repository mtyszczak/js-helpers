[![pipeline status](https://gitlab.com/mtyszczak/js-helpers/badges/master/pipeline.svg)](https://gitlab.com/mtyszczak/js-helpers/-/commits/master)
[![coverage report](https://gitlab.com/mtyszczak/js-helpers/badges/master/coverage.svg)](https://gitlab.com/mtyszczak/js-helpers/-/commits/master)
[![License GNU GPL v3](https://img.shields.io/badge/License-GNU%20GPL%20v3-red.svg)](LICENSE)
[![Version 0.0.1](https://img.shields.io/badge/Version-0.0.1-abc.svg)](https://gitlab.com/mtyszczak/js-helpers/-/commits/master)

# js-helpers
Complete tool set of JavaScript helpers written in TypeScript.
This project is fully created with &lt;3 by Mateusz Tyszczak.
Some of the components are based on the ideas from Boost libraries (yes - C++ containers in JS :OO)

## Running
Documentation is supposed to work on every device that has a built-in web browser that supports HTML5, CSS3 and ES2017.
Simple open the file named `index.html` or run a server with the root directory of this project.

## Compiling
Simple run `build.sh` script located in the root directory of this project to install libraries of your choice. Use `--help` option to display builder help.

## Editing

### Website files layout
All of the page assets can be found in the `asasets` directory.
HTML files are just documentation of the libraries.

### JavaScript files copying
Some of the typescript files are **dependent** from each other (if not so it will be mentioned in the documentation).

## Libraries List
|  **Name**   | **Version** | **Status**  |               **Description**                |   **Dependencies**  |
|-------------|-------------|-------------|----------------------------------------------|---------------------|
|  assert     |    0.0.1    | Development |             Assertion interface              | *structs*           |
|  graphics   |    0.0.1    | Development |             Graphics interface               | *assert*            |
|  strings    |    0.0.1    | Development | String utils such as: hashing functions etc. |                     |
|  structs    |    0.0.1    | Development |      JS "STL" structs and containers         | *strings*, *assert* |
|   utils     |    0.0.1    | Development |    Utils for typescript and javascript       |                     |
|  version    |    0.0.1    | Development | Version parser, compare, stringify and more  | *assert*            |

## TODOs and ideas

- [ ] Math library (like vectors etc.)

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)