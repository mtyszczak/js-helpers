import { AssertionError } from "../src/assert";
import { COLOR_MIN_VALUE, COLOR_MAX_VALUE, Color, HEX, HEXT, RGB, RGBA, HSL, HSLA, CMYK, Predefined, ColorType, HslaBasedColors, RgbaBasedColors } from "../src/graphics";

describe("graphics library", () => {

  test("color values", () => {
    expect( COLOR_MIN_VALUE ).toBe( 0 );
    try {
      Color.checkColorValue( COLOR_MIN_VALUE - 1 );
      expect( "This expectation should never occur" ).toBeUndefined();
    } catch(e) {
      expect(e.name).toBe( "AssertionError" );
    }
    expect( COLOR_MAX_VALUE ).toBe( 255 );
    try {
      Color.checkColorValue( COLOR_MAX_VALUE + 1 );
      expect( "This expectation should never occur" ).toBeUndefined();
    } catch(e) {
      expect(e.name).toBe( "AssertionError" );
    }
  });

  test("Color initialization", () => {
    const strColor = "#abcdef";
    const c = new Color( strColor );
    expect( c.getRaw() ).toBe( strColor );
  });

  describe("string to color type", () => {
    test( "HEX", () => {
      expect( Color.getColorType( "#aabbcc" ) ).toBe( ColorType.HEX );
    });
    test( "HEXT", () => {
      expect( Color.getColorType( "#aabbcc55" ) ).toBe( ColorType.HEXT );
    });
    test( "RGB", () => {
      expect( Color.getColorType( "rgb( 10, 50%, 20 )" ) ).toBe( ColorType.RGB );
    });
    test( "RGBA", () => {
      expect( Color.getColorType( "rgba( 10, 50%, 20, 30% )" ) ).toBe( ColorType.RGBA );
    });
    test( "HSL", () => {
      expect( Color.getColorType( "hsl( 10, 50%, 20 )" ) ).toBe( ColorType.HSL );
    });
    test( "HSLA", () => {
      expect( Color.getColorType( "hsla( 10, 50%, 20, 30% )" ) ).toBe( ColorType.HSLA );
    });
    test( "CMYK", () => {
      expect( Color.getColorType( "device-cmyk( 10 50% 20 30% / 50% )" ) ).toBe( ColorType.CMYK );
    });
    test( "Predefined", () => {
      expect( Color.getColorType( "blue" ) ).toBe( ColorType.PREDEFINED );
    });
  });

  describe("string to color", () => {
    test( "HEX", () => {
      expect( Color.parseColor( "#aabbcc" ) ).toStrictEqual( { r: 0xaa, g: 0xbb, b: 0xcc, a:COLOR_MAX_VALUE } as HEX );
    });
    test( "HEXT", () => {
      expect( Color.parseColor( "#aabbcc55" ) ).toStrictEqual( { r: 0xaa, g: 0xbb, b: 0xcc, a: 0x55 } as HEXT );
    });
    test( "RGB", () => {
      expect( Color.parseColor( "rgb( 10, 50%, 20 )" ) ).toStrictEqual( { r: 10, g: 0.5*COLOR_MAX_VALUE, b: 20, a:COLOR_MAX_VALUE } as RGB );
    });
    test( "RGBA", () => {
      expect( Color.parseColor( "rgba( 10, 50%, 20, 30% )" ) ).toStrictEqual( { r: 10, g: 0.5*COLOR_MAX_VALUE, b: 20, a: 0.3*COLOR_MAX_VALUE } as RGBA );
    });
    test( "HSL", () => {
      expect( Color.parseColor( "hsl( 10, 50%, 20 )" ) ).toStrictEqual( { h: 10, s: 0.5*COLOR_MAX_VALUE, l: 20, a:COLOR_MAX_VALUE } as HSL );
    });
    test( "HSLA", () => {
      expect( Color.parseColor( "hsla( 10, 50%, 20, 30% )" ) ).toStrictEqual( { h: 10, s: 0.5*COLOR_MAX_VALUE, l: 20, a: 0.3*COLOR_MAX_VALUE } as HSLA );
    });
    test( "CMYK", () => {
      expect( Color.parseColor( "device-cmyk( 10 50% 20 30% / 50% )" ) ).toStrictEqual( { c: 10, m: 0.5*COLOR_MAX_VALUE, y: 20, k: 0.3*COLOR_MAX_VALUE, a: 0.5*COLOR_MAX_VALUE } as CMYK );
    });
    test( "Predefined", () => {
      expect( Color.parseColor( "blue" ) ).toStrictEqual( { value: "blue" } as Predefined );
    });
  });

});