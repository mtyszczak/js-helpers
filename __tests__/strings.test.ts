import "../src/strings";

describe("strings library", () => {
  test("string hashing", () => {

    expect( String.prototype.hashCode ).toBeDefined();

    expect( "Hello, world!".hashCode() ).toBe( -1880044555 );

  });
});