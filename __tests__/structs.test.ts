import { Pair, TypesEnum, UniversalIndexHashHolder } from '../src/structs';

describe("structs library", () => {

  describe("TypesEnum", () => {

    test("function", () => {
      function myCustomFunction() {}
      expect( TypesEnum.stringify( myCustomFunction ) ).toBe( "myCustomFunction" );
    });

    test("boolean", () => {
      expect( TypesEnum.stringify( true ) ).toBe( "true" );
      expect( TypesEnum.stringify( false ) ).toBe( "false" );
    });

    test("string", () => {
      expect( TypesEnum.stringify( "alice" ) ).toBe( "alice" );
    });

    test("array", () => {
      expect( TypesEnum.stringify( [ "bob", 1, 2.3, true, {} ] ) ).toBe( "[\"bob\",1,2.3,true,{}]" );
    });

    test("object", () => {
      expect( TypesEnum.stringify( {a:1,b:2,c:3} ) ).toBe( "{\"a\":1,\"b\":2,\"c\":3}" );
    });

    test("undefined", () => {
      expect( TypesEnum.stringify( undefined ) ).toBe( "undefined" );
    });

    test("symbol", () => {
      expect( TypesEnum.stringify( Symbol.for( "Alice" ) ) ).toBe( "Symbol(Alice)" );
    });

    test("number", () => {
      expect( TypesEnum.stringify( 10 ) ).toBe( "10" );
      expect( TypesEnum.stringify( 3.141592 ) ).toBe( "3.141592" );
      expect( TypesEnum.stringify( BigInt("13792932114622731832189") ) ).toBe( "13792932114622731832189" );
    });

    test("Custom type", () => {
      class MyType {
        data:number;
        constructor( data:number ) { this.data = data; }
        toString(type:MyType):string { return `${type.data}`; }
      }

      const myTypeIndex = TypesEnum.addType( MyType );
      TypesEnum.addStringifier( myTypeIndex, MyType.prototype.toString );

      const t = new MyType( 10 );

      expect( TypesEnum.stringify( t ) ).toEqual( "10" );
    });
  });

  test("Pair", () => {
    const pair1 = new Pair< Number, String > ( 10, "Hello, world!" );

    expect( pair1.first  ).toEqual( 10 );
    expect( pair1.second ).toEqual( "Hello, world!" );
  });

  test("UniversalIndexHashHolder", () => {
    const data = {a:1,b:2,c:3};
    const holder = new UniversalIndexHashHolder< object >( data );

    expect( holder.getData() ).toBe( data );
    expect( holder.getIndex() ).toBe( -1222372670 );
  });

});