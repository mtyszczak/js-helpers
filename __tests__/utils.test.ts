import { staticImplements } from "../src/utils";

describe("utils library", () => {
  test("static implements", () => {

    interface StaticBase {
      /**
       * Member to be implemented as static
       * @type {string}
       */
      member:string;
    }

    const MEMBER_CONTENT = "content";

    @staticImplements< StaticBase >()
    class Derived {
      public static member:string = MEMBER_CONTENT;
    }

    expect( Derived.member ).toBe( MEMBER_CONTENT );

  });
});