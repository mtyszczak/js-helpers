import { Version } from '../src/version';

describe("version library", () => {
  test("Version parsing", () => {

    const v1 = new Version( 1, 0, 1 );
    expect( Version.stringify(v1) ).toBe( "1.0.1" );

    const v2 = Version.parse( Version.stringify( v1 ) );
    expect( v2.compare( v1 ) ).toBeFalsy(); // compare equal == 0

  });
});