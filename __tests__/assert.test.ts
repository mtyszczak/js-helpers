import { staticAssert, AssertionError } from '../src/assert';

describe("assert library", () => {
  test("AssertionError error compatibility", () => {

    const e = new AssertionError( "message ${a} ${b} ${c}", "description", { a: 1, b: 2, c: 3 } );

    expect( e ).toBeInstanceOf( Error );

    expect( e.name ).toBe( "AssertionError" );
    expect( e.message ).toBe( "message 1 2 3"
                            + "\nDescription: description"
                            + "\nresolvers data: {\"a\":1,\"b\":2,\"c\":3}" );
    expect( e.stack ).toBeDefined();
  });

  test("staticAssert error throwing", () => {

    try {
      staticAssert( false, "test assert: ${p}", { p: 2 } );
      expect( "This expectation should never occur" ).toBeUndefined();
    } catch ( e ) {
        expect( e ).toBeInstanceOf( AssertionError );
        expect( e.message ).toBe( "test assert: 2"
                                + "\nDescription: Static assertion failed"
                                + "\nresolvers data: {\"p\":2}" );
    }

  });
});