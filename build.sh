#!/bin/bash

OUTDIR=.
LIBS=( assert version strings structs graphics utils )
VERSION="0.1"
TSC_OPTIONS="--alwaysStrict --target ES2017 --module commonjs --pretty --skipLibCheck --experimentalDecorators"

# Internal flags
flag_show_libraries=
flag_gen_docs=
flag_run_tests=
flag_skip_building=
flag_use_docker=
want_help=
concatenate_lib=

bin_docker_image="docker run --rm --volume $(pwd):/app sandrokeil/typescript"

tsc_bin=tsc
npx_bin=npx
npm_bin=npm
node_bin=node


for option
do
    case $option in

    -help | --help | -h)
      want_help=yes ;;

    -concatenate-libraries | --concatenate-libraries | -c)
      concatenate_lib=yes ;;

    -generate-docs | --generate-docs | -d)
      flag_gen_docs=yes ;;

    -use-docker | --use-docker )
      flag_use_docker=yes ;;

    -skip-building | --skip-building | -s)
      flag_skip_building=yes ;;

    -outdir=* | --outdir=*)
      OUTDIR=`expr "x$option" : "x-*outdir=\(.*\)"`
      ;;

    -show-libraries | --show-libraries )
      flag_show_libraries=yes
      ;;

    -run-tests | --run-tests )
      flag_run_tests=yes
      ;;

    -with-libraries=* | --with-libraries=* )
      library_list=`expr "x$option" : "x-*with-libraries=\(.*\)"`
      if test "$library_list" != "all"; then
          LIBS=()
          old_IFS=$IFS
          IFS=,
          for library in $library_list
          do
            LIBS+=( "$library" )

          done
          IFS=$old_IFS
      fi
      ;;

    -without-libraries=* | --without-libraries=* )
      library_list=`expr "x$option" : "x-*without-libraries=\(.*\)"`
      old_IFS=$IFS
      IFS=,
      for library in $library_list
      do
          LIBS=( "${LIBS[@]/$library}" )
      done
      IFS=$old_IFS
      ;;

    -*)
      { echo "error: unrecognized option: $option
Try \`$0 --help' for more information." >&2
      { (exit 1); exit 1; }; }
      ;;

    esac
done

if test "x$want_help" = xyes; then
  cat <<EOF
\`./build.sh\` builds the js-helpers libraries

Usage: $0 [OPTION]...

Defaults for the options are specified in brackets.

Configuration:
  -h, --help                   display this help and exit
  --show-libraries             show available libraries
  --with-libraries=list        build only a particular set of libraries,
                               describing using either a comma-separated list of
                               library names or "all"
  --without-libraries=list     build all libraries except the ones listed
  -c, --concatenate-libraries  concatenate all the selected libraries into
                               the one file. Uses 'commonjs' module. Output library
                               name is "helpers"
  -d, --generate-docs          regenerate documentation (HTML format in docs dir)

Build options:
  -s, --skip-building          skips building javascript source (exit after docs
                               generation and running all of the tests)
  --use-docker                 uses docker sandrokeil/typescript image instead of
                               local typescript and nodejs commands

Installation directories:
  --outdir=OUTDIR              install helper-js into the given OUTDIR

Testing
  --run-tests                  runs tests in tests directory

EOF
fi
test -n "$want_help" && exit 0

my_dir=$(dirname "$0")

# If there is a list of libraries
if test "x$flag_show_libraries" = xyes; then
  cat <<EOF

The following libraries are available:

${LIBS[@]}

EOF
  exit 0
fi

if test "x$flag_use_docker" = xyes; then
cat <<EOF
Using docker image: $bin_docker_image
EOF
  tsc_bin="$bin_docker_image $tsc_bin"
  npm_bin="$bin_docker_image $npm_bin"
  npx_bin="$bin_docker_image $npx_bin"
  node_bin="$bin_docker_image $node_bin"

fi

if test "x$flag_gen_docs" = xyes; then

  cat <<EOF
Regenerating documentation...

EOF

if test "x$flag_use_docker" != xyes; then
  # Find npm and nodejs
  result=`npm -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
npm not found! Try installing it with \`sudo apt install npm\` Exitting...

EOF
    exit 1
  fi
  result=`node -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
node not found! Try installing it with \`sudo apt install nodejs\` Exitting...

EOF
    exit 1
  fi
fi

  bash -c "$npm_bin install"
  if [ -d wiki ]; then rm -r wiki/*;
  else mkdir wiki; fi
  bash -c "$npm_bin run typedoc"
fi

if test "x$flag_run_tests" = xyes; then

  cat <<EOF
Running tests...


EOF

if test "x$flag_use_docker" != xyes; then
  # Find npm and nodejs
  result=`npm -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
npm not found! Try installing it with \`sudo apt install npm\` Exitting...

EOF
    exit 1
  fi
  result=`node -v > /dev/null 2>&1`
  if [ "$?" -ne "0" ]; then
    cat <<EOF
node not found! Try installing it with \`sudo apt install nodejs\` Exitting...

EOF
    exit 1
  fi
fi

  bash -c "$npm_bin install"
  bash -c "$npx_bin jest --runInBand"

fi

if test "x$flag_skip_building" = xyes; then
  cat << EOF
Skipping building due to the -s flag enabled
EOF
  exit 0;
fi

if test "x$flag_use_docker" != xyes; then
# Find TypeScript compiler
result=`tsc -v > /dev/null 2>&1`
if [ "$?" -ne "0" ]; then
  cat <<EOF
TypeScript compiler not found! Try installing it with \`sudo apt install node-typescript\` Exitting...

EOF
  exit 1
fi

cat <<EOF
Using $(whereis tsc)
EOF
fi
cat <<EOF
Compiling libs: ${LIBS[@]}

EOF

FIN_LIBS=$LIBS # Raw copy of the selected libs for the further parsing

count=0
for i in ${LIBS[@]}
do
  LIBS[$count]="src/${LIBS[$count]}.ts"
  count=$((count + 1))
done

TSC_OPTIONS="$TSC_OPTIONS --outDir "$(pwd)/$OUTDIR" ${LIBS[@]}"

cat <<EOF
$tsc_bin $TSC_OPTIONS
EOF
bash -c "$tsc_bin $TSC_OPTIONS"

if [ "$?" -ne "0" ]; then
  cat <<EOF
An error occured while compiling!

EOF
  exit 1
else

  if test "x$concatenate_lib" = xyes; then
    REALPATH=./$(realpath --relative-base=. $OUTDIR)
    cat <<EOF
$npx_bin webpack-cli --mode production --output-environment-const --output-library-type umd --output-library-name helpers -o "$OUTDIR" --output-filename "helpers-$VERSION.js" "$REALPATH/${FIN_LIBS[0]}.js"
EOF
    bash -c "$npx_bin webpack-cli --mode production --output-environment-const --output-library-type umd --output-library-name helpers -o \"$OUTDIR\" --output-filename \"helpers-$VERSION.js\" \"$REALPATH/${FIN_LIBS[0]}.js\""
  fi

  if [ "$?" -ne "0" ]; then
    cat <<EOF
  An error occured while concatenating!

EOF
    exit 1
fi

  cat << EOF

Build process has succesfully ended. Search for the generated javascript files in the provided "$OUTDIR" directory.

EOF
fi

exit 0
