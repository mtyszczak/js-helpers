/**
 * assert.ts - Assertion interface
 *
 * @version 0.0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

import { TypesEnum } from './structs';

/**
 * Extension to the Error class. Adds possibility to display additional description in try catch block
 */
export class AssertionError extends Error {
  /**
   * Creates new instance of an AssertionError
   * @param {string} message message for the error to be thrown
   * @param {string} description optional description returned from \b describe
   * @param {object} resolvers object with members that will be replaced in message, e.g. message: "${a1}, World!" and resolvers: {a1:"Hello"}. Output: "Hello, World!"
   *
   * @public
   * @constructor
   */
  public constructor( message: string, description:string = "", resolvers:object = {} ) {
    super(
      ( Object.keys( resolvers ).forEach( (value) => {
        message = message.replace( `\${${value}}`, TypesEnum.stringify( resolvers[value] ) )
      }), message += `\nDescription: ${description}\nresolvers data: ${JSON.stringify(resolvers)}` )
    );

    // Set the prototype explicitly (solution transpiled to ES5)
    (<any>Object).setPrototypeOf(this, AssertionError.prototype)
  }

  /**
   * Name of this error
   * @type {string}
   * @public
   * @readonly
   */
  public readonly name:string = "AssertionError";
}

/**
 * Performs a static assertion check without creating a new object of type Assert
 *
 * @param {boolean} statement check statement
 * @param {string} message optional message to describe error
 * @param {object} resolvers object with members that will be replaced in message, e.g. message: "${a1}, World!" and resolvers: {a1:"Hello"}. Output: "Hello, World!"
 * @throws {AssertionError} when statement evaluates to false
 */
export const staticAssert = ( statement:boolean, message:string = "", resolvers:object = {} ):void => {
  if( !statement ) throw new AssertionError( message, "Static assertion failed", resolvers );
}

// TODO: Assert class
