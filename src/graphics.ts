/**
 * graphics.ts - Graphics interface
 *
 * @version 0.0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

import { staticAssert } from './assert';

 export enum ColorType {
   HEX,
   HEXT, /* Hex with transparency */
   RGB,
   RGBA,
   HSL,
   HSLA,
   CMYK,
   PREDEFINED
 }

 /**
  * @interface
  */
 export interface RgbaBasedColors {
   /// Red channel
   r:number;
   /// Green channel
   g:number;
   /// Blue channel
   b:number;
   /// Alpha channel
   a:number;
 }

 /**
  * @interface
  */
 export interface HslaBasedColors {
   /// Hue channel
   h:number;
   /// Saturation channel
   s:number;
   /// Lightness channel
   l:number;
   /// Alpha channel
   a:number;
 }

 /**
  * @interface
  */
 export interface CmykBasedColors {
   /// Cyan channel
   c:number;
   /// Magenta channel
   m:number;
   /// Yellow channel
   y:number;
   /// Black channel
   k:number;
   /// Alpha channel
   a:number;
 }

 /**
  * HEX representation of colors
  * @interface
  * @extends RgbaBasedColors
  */
 export interface HEX  extends RgbaBasedColors {}
 /**
  * HEX with transparency representation of colors
  * @interface
  * @extends RgbaBasedColors
  */
 export interface HEXT extends RgbaBasedColors {}
 /**
  * RGB representation of colors
  * @interface
  * @extends RgbaBasedColors
  */
 export interface RGB  extends RgbaBasedColors {}
 /**
  * RGBA representation of colors
  * @interface
  * @extends RgbaBasedColors
  */
 export interface RGBA extends RgbaBasedColors {}
 /**
  * HSL representation of colors
  * @interface
  * @extends HslaBasedColors
  */
 export interface HSL  extends HslaBasedColors {}
 /**
  * HSLA representation of colors
  * @interface
  * @extends HslaBasedColors
  */
 export interface HSLA extends HslaBasedColors {}
 /**
  * CMYK representation of colors
  * @interface
  * @extends CmykBasedColors
  */
 export interface CMYK extends CmykBasedColors {}
 /**
  * Holds raw predefined color value
  * @interface
  */
 export interface Predefined {
   /**
    * Raw predefined color value
    * @type {string}
    */
   value:string;
 }

 /**
  * Minimum color value
  * @constant
  * @type {number}
  */
 export const COLOR_MIN_VALUE:number = 0;
 /**
  * Maximum color value
  * @constant
  * @type {number}
  */
 export const COLOR_MAX_VALUE:number = 255;


 /**
  * Color class implementing various color conversions, checks and type detection
  *
  * Note: Class compatible only with HEX, RGB, HSL, CMYK (and their transparent variation)
  * Currently icc-colors - color profiles are not supported
  *
  * Class instances are used just as wrappers to the string representation of the color
  *
  * @class
  */
 export class Color {

   /**
    * Holds raw color data (in string type)
    * @type {string}
    * @private
    */
   private data:string;

   /**
    * Constructs new Color from the given string
    * @param {string} data color in any type compatible with ColorType enum
    * @public
    * @constructor
    */
   public constructor( data:string = "transparent" ) {
     this.data = data;
   }

   /**
    * Returns current raw color data
    * @returns {string} current raw color data
    * @public
    */
   public getRaw():string {
     return this.data;
   }

   /**
    * Checks given number for the integrity of its value
    * @param {number} data given number to be checked
    * @returns {number} data itself after passing the checks
    * @public
    * @static
    */
   public static checkColorValue( data:number ):number {
     staticAssert( !isNaN( data ), "Color value must not be NaN", { data } );
     staticAssert( data >= COLOR_MIN_VALUE && data <= COLOR_MAX_VALUE,
                   "Color value must fit in range ${COLOR_MIN_VALUE} to ${COLOR_MAX_VALUE}",
                   { COLOR_MIN_VALUE, COLOR_MAX_VALUE, data }
                 );

     return data;
   }

   /**
    * Converts example string: "143, 10%, 12, .3" into the array of numbers like: [ 143, 25.5, 12, .3 ]
    * @param {string} data data to be parsed
    * @param {string} separator separator to be used when splitting string (defaults to ',')
    * @returns {Array<number>} parsed array of numbers
    * @private
    * @static
    */
   private static parseColorStringIntoArray( data:string, separator:string = ',' ):Array<number> {
     return data.split( separator )
                .map( str => str.trim() )
                .map( str => str.endsWith( '%' ) ?
                               ( 0.01 * Number.parseFloat( str.substr( 0, str.length - 1 ) ) ) * COLOR_MAX_VALUE
                             : Number.parseFloat( str )
                     )
                .map( str => this.checkColorValue( str ) );
   }

  // TODO: Color conversions

   /**
    * Parses given HEX string into the HEX object
    * @param {string} data data to be parsed
    * @returns {HEX} hex representation of the data
    * @public
    * @static
    */
   public static parseHEX( data:string ):HEX {
     data.trim();
     staticAssert( data.startsWith('#'), "HEX color must start with a '#'", { data } );
     staticAssert( data.length == 4 || data.length == 7, "HEX color must be either 4 or 7 characters length (including '#')", { data } );
     let values:HEX = {} as HEX;
     values.a = COLOR_MAX_VALUE; // HEX (without an alpha channel) is always non transparent
     if( data.length == 4 )
     {
       values.r = this.checkColorValue( Number.parseInt( data.charAt( 1 ) + data.charAt( 1 ), 16 ) );
       values.g = this.checkColorValue( Number.parseInt( data.charAt( 2 ) + data.charAt( 2 ), 16 ) );
       values.b = this.checkColorValue( Number.parseInt( data.charAt( 3 ) + data.charAt( 3 ), 16 ) );
     } else { // Only data.length == 7 is now possible due to the previous assertion
       values.r = this.checkColorValue( Number.parseInt( data.substr( 1, 2 ), 16 ) );
       values.g = this.checkColorValue( Number.parseInt( data.substr( 3, 2 ), 16 ) );
       values.b = this.checkColorValue( Number.parseInt( data.substr( 5, 2 ), 16 ) );
     }
     return values;
   }

   /**
    * Parses given HEX (with transparency) string into the HEXT object
    * @param {string} data data to be parsed
    * @returns {HEXT} hext representation of the data
    * @public
    * @static
    */
   public static parseHEXT( data:string ):HEXT {
     data.trim();
     staticAssert( data.startsWith('#'), "HEXT color must start with a '#'", { data } );
     staticAssert( data.length == 5 || data.length == 9, "HEXT color must be either 5 or 9 characters length (including '#')", { data } );
     let values:HEXT = {} as HEXT;
     if( data.length == 5 )
     {
       values.r = this.checkColorValue( Number.parseInt( data.charAt( 1 ) + data.charAt( 1 ), 16 ) );
       values.g = this.checkColorValue( Number.parseInt( data.charAt( 2 ) + data.charAt( 2 ), 16 ) );
       values.b = this.checkColorValue( Number.parseInt( data.charAt( 3 ) + data.charAt( 3 ), 16 ) );
       values.a = this.checkColorValue( Number.parseInt( data.charAt( 4 ) + data.charAt( 4 ), 16 ) );
     } else { // Only data.length == 9 is now possible due to the previous assertion
       values.r = this.checkColorValue( Number.parseInt( data.substr( 1, 2 ), 16 ) );
       values.g = this.checkColorValue( Number.parseInt( data.substr( 3, 2 ), 16 ) );
       values.b = this.checkColorValue( Number.parseInt( data.substr( 5, 2 ), 16 ) );
       values.a = this.checkColorValue( Number.parseInt( data.substr( 7, 2 ), 16 ) );
     }
     return values;
   }

   /**
    * Parses given RGB string into the RGB object
    * @param {string} data data to be parsed
    * @returns {RGB} rgb representation of the data
    * @public
    * @static
    */
   public static parseRGB( data:string ):RGB {
     data.trim();
     staticAssert( data.toLowerCase().startsWith('rgb'), "RGB color must start with an 'rgb'", { data } );
     data = data.substr( 3 ).trim();
     staticAssert( data.charAt( 0 ) == '(', "RGB color values must be enclosed in parentheses", { data } );
     let workingValues:Array<number> = this.parseColorStringIntoArray( data.substr( 1, data.length - 2 ) );
     staticAssert( workingValues.length == 3, "RGB color type must have 3 floating type or percentage values inside their parentheses", { data } );
     return {
       r: workingValues[0],
       g: workingValues[1],
       b: workingValues[2],
       a: COLOR_MAX_VALUE // RGB (without an alpha channel) is always non transparent
     };
   }

   /**
    * Parses given RGBA string into the RGBA object
    * @param {string} data data to be parsed
    * @returns {RGBA} rgba representation of the data
    * @public
    * @static
    */
   public static parseRGBA( data:string ):RGBA {
     data.trim();
     staticAssert( data.toLowerCase().startsWith('rgba'), "RGBA color must start with an 'rgba'", { data } );
     data = data.substr( 4 ).trim();
     staticAssert( data.charAt( 0 ) == '(', "RGB color values must be enclosed in parentheses", { data } );
     let workingValues:Array<number> = this.parseColorStringIntoArray( data.substr( 1, data.length - 2 ) );
     staticAssert( workingValues.length == 4, "RGBA color type must have 4 floating type or percentage values inside their parentheses", { data } );
     return {
       r: workingValues[0],
       g: workingValues[1],
       b: workingValues[2],
       a: workingValues[3]
     };
   }

   /**
    * Parses given HSL string into the HSL object
    * @param {string} data data to be parsed
    * @returns {HSL} hsl representation of the data
    * @public
    * @static
    */
   public static parseHSL( data:string ):HSL {
     data.trim();
     staticAssert( data.toLowerCase().startsWith('hsl'), "HSL color must start with an 'hsl'", { data } );
     data = data.substr( 3 ).trim();
     staticAssert( data.charAt( 0 ) == '(', "HSL color values must be enclosed in parentheses", { data } );
     let workingValues:Array<number> = this.parseColorStringIntoArray( data.substr( 1, data.length - 2 ) );
     staticAssert( workingValues.length == 3, "HSL color type must have 3 floating type or percentage values inside their parentheses", { data } );
     return {
       h: workingValues[0],
       s: workingValues[1],
       l: workingValues[2],
       a: COLOR_MAX_VALUE // HSL (without an alpha channel) is always non transparent
     };
   }

   /**
    * Parses given HSLA string into the HSLA object
    * @param {string} data data to be parsed
    * @returns {HSLA} hsla representation of the data
    * @public
    * @static
    */
   public static parseHSLA( data:string ):HSLA {
     data.trim();
     staticAssert( data.toLowerCase().startsWith('hsl'), "HSL color must start with an 'hsl'", { data } );
     data = data.substr( 4 ).trim();
     staticAssert( data.charAt( 0 ) == '(', "HSL color values must be enclosed in parentheses", { data } );
     let workingValues:Array<number> = this.parseColorStringIntoArray( data.substr( 1, data.length - 2 ) );
     staticAssert( workingValues.length == 4, "HSLA color type must have 4 floating type or percentage values inside their parentheses", { data } );
     return {
       h: workingValues[0],
       s: workingValues[1],
       l: workingValues[2],
       a: workingValues[3]
     };
   }

   /**
    * Parses given CMYK string into the CMYK object
    * @param {string} data data to be parsed
    * @returns {CMYK} cmyk representation of the data
    * @public
    * @static
    */
   public static parseCMYK( data:string ):CMYK {
     data.trim();
     staticAssert( data.toLowerCase().startsWith('device-cmyk'), "CMYK color must start with a 'device-cmyk'", { data } );
     data = data.substr( 11 ).trim();
     staticAssert( data.charAt( 0 ) == '(', "HSL color values must be enclosed in parentheses", { data } );
     const dataChannelsFilter = data.substr( 1, data.length - 2 ).split( '/' );
     let workingValues:Array<number> = this.parseColorStringIntoArray( dataChannelsFilter[0].trim(), ' ' );
     staticAssert( workingValues.length == 4, "CMYK color type must have 4 floating type or percentage values inside their parentheses", { data } );
     return {
       c: workingValues[0],
       m: workingValues[1],
       y: workingValues[2],
       k: workingValues[3],
       a: this.parseColorStringIntoArray( dataChannelsFilter[1].trim(), '/' )[0]
     };
   }

   /**
    * Parses given predefined color string into the Predefined object
    * @param {string} data data to be parsed
    * @returns {Predefined} predefined representation of the data
    * @public
    * @static
    */
   public static parsePredefined( data:string ):Predefined {
     data.trim();
     staticAssert( data.length > 0, "Predefined string must not be empty", { data } );
     return { value: data };
   }

   /**
    * Automatically parses string into an object
    * @param {string} data data to be parsed
    * @returns {RgbaBasedColors|HslaBasedColors|CmykBasedColors|Predefined} object representation of the data
    * @public
    * @static
    */
   public static parseColor( data:string ):RgbaBasedColors|HslaBasedColors|CmykBasedColors|Predefined {
     switch( this.getColorType( data ) ) {
       case ColorType.HEX:  return this.parseHEX( data );
       case ColorType.HEXT: return this.parseHEXT( data );
       case ColorType.RGB:  return this.parseRGB( data );
       case ColorType.RGBA: return this.parseRGBA( data );
       case ColorType.HSL:  return this.parseHSL( data );
       case ColorType.HSLA: return this.parseHSLA( data );
       case ColorType.CMYK: return this.parseCMYK( data );
       default:
       case ColorType.PREDEFINED: return this.parsePredefined( data );
     }
   }

   /**
    * Returns approx color type based on the value given as an argument
    * @param {number} data data to be parsed
    * @returns {RgbaBasedColors|Predefined} expected color type parsed from the data
    * @public
    * @static
    */
   public static getColorType( data:string ):ColorType {
     data.trim();
     const indexOfFirstParentheses = data.indexOf( '(' );
     switch( data.charAt( 0 ).toLowerCase() ) { // Wish i could write this parser using pointers in c++ ;)
       case '#':
         if( data.length == 4 || data.length == 7 ) // e.g. #000 or #000000
           return ColorType.HEX;
         else if( data.length == 5 || data.length == 9 ) // e.g. #0000 or #00000000
           return ColorType.HEXT;
         staticAssert( false, "Hex color type must be either 4, 5, 7 or 9 characters length", { data } );
         break;
       case 'r':
         if( data.substr( 1, 2 ).toLowerCase() !== "gb" ) break;
         if( data.charAt( 3 ).toLowerCase() == 'a' ) {
           staticAssert( data.substr( indexOfFirstParentheses+1, data.length - indexOfFirstParentheses - 2 )
                             .split( ',' )
                             .map( str => str.trim() )
                             .map( str => str.endsWith( '%' ) ? ( 0.01 * Number.parseFloat( str.substr( 0, str.length - 1 ) ) ) * COLOR_MAX_VALUE + "" : str )
                             .map( str => this.checkColorValue( Number.parseFloat( str ) ) )
                             .length == 4, "RGBA color type must have 4 floating type values inside their parentheses", { data } );
             return ColorType.RGBA;
         } else {
           staticAssert( data.substr( indexOfFirstParentheses+1, data.length - indexOfFirstParentheses - 2 )
                             .split( ',' )
                             .map( str => str.trim() )
                             .map( str => str.endsWith( '%' ) ? ( 0.01 * Number.parseFloat( str.substr( 0, str.length - 1 ) ) ) * COLOR_MAX_VALUE + "" : str )
                             .map( str => this.checkColorValue( Number.parseFloat( str ) ) )
                             .length == 3, "RGB color type must have 3 floating type values inside their parentheses", { data } );
             return ColorType.RGB;
         }
       case 'h':
         if( data.substr( 1, 2 ).toLowerCase() !== "sl" ) break;
         if( data.charAt( 3 ).toLowerCase() == 'a' ) {
           staticAssert( data.substr( indexOfFirstParentheses+1, data.length - indexOfFirstParentheses - 2 )
                             .split( ',' )
                             .map( str => str.trim() )
                             .map( str => str.endsWith( '%' ) ? ( 0.01 * Number.parseFloat( str.substr( 0, str.length - 1 ) ) ) * COLOR_MAX_VALUE + "" : str )
                             .map( str => this.checkColorValue( Number.parseFloat( str ) ) )
                             .length == 4, "HSLA color type must have 4 floating type values inside their parentheses", { data } );
             return ColorType.HSLA;
         } else {
           staticAssert( data.substr( indexOfFirstParentheses+1, data.length - indexOfFirstParentheses - 2 )
                             .split( ',' )
                             .map( str => str.trim() )
                             .map( str => str.endsWith( '%' ) ? ( 0.01 * Number.parseFloat( str.substr( 0, str.length - 1 ) ) ) * COLOR_MAX_VALUE + "" : str )
                             .map( str => this.checkColorValue( Number.parseFloat( str ) ) )
                             .length == 3, "HSL color type must have 3 floating type values inside their parentheses", { data } );
             return ColorType.HSL;
         }
       case 'd':
         if( data.substr( 1, 10 ).toLowerCase() !== "evice-cmyk" ) break;
         staticAssert( data.substr( indexOfFirstParentheses+1, data.length - indexOfFirstParentheses - 2 )
                           .split( '/' )[0].trim()
                           .split( ' ' )
                           .map( str => str.trim() )
                           .map( str => str.endsWith( '%' ) ? ( 0.01 * Number.parseFloat( str.substr( 0, str.length - 1 ) ) ) * COLOR_MAX_VALUE + "" : str )
                           .map( str => this.checkColorValue( Number.parseFloat( str ) ) )
                           .length == 4, "CMYK color type must have 4 floating type values inside their parentheses", { data } );
           return ColorType.CMYK;
       default: break;
     }
     return ColorType.PREDEFINED;
   }
 }