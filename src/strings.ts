/**
 * strings.ts - String utils such as: hashing functions etc.
 *
 * @version 0.0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

declare global {
  interface String {
    /**
     * Hashes string
     * @returns {number} string hash
     */
    hashCode(): number;
  }
}

// Source: http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
String.prototype.hashCode = function():number {
  let hash:number = 0, i:number, chr:number;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; ++i) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

export {};