/**
 * utils.ts - Utils for typescript and javascript (like e.g. experimental class decorators)
 *
 * @version 0.0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

/**
 * Implements static interface
 * For use case see: https://github.com/Microsoft/TypeScript/issues/13462#issuecomment-295685298
 * @returns {<U extends T>(constructor: U) => void} static implementation of interface
 */
export const staticImplements = <T>():<U extends T>(constructor: U) => void => {
  return <U extends T>(constructor: U) => {constructor};
};

// TODO: Singletone
