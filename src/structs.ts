/**
 * structs.ts - JS "STL" structs and containers
 *
 * @version 0.0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

import { staticAssert } from './assert';
import './strings';

/**
 * Object containing most common types.
 * @constant
 */
export const TypesEnum = {
  /**
   * stringifies any type
   * @param {any} type type to be parsed
   * @returns {string} stringified type
   */
  stringify(type:any):string {
    const parsed = this.getType( type );
    switch( parsed ) {
      case this.function:
        return type.name || "";
      case this.object: case this.array:
        return JSON.stringify(type) || "";
      case this.string: case this.boolean:
      case this.symbol: case this.number:
        return type.toString() || "";
      case this.undefined:
        return "undefined";
      default:
        return this[`stringify_${parsed}`](type);
    }
  },

  /**
   * Adds custom function or class type into the TypesEnum
   * @param {Function} type class or function
   * @returns {number} number assigned to the given type in TypesEnum
   */
  addType(type:Function):number {
    if( typeof this[ type.name ] !== "undefined" )
      staticAssert( false, "Typename already taken with num: ${num}", { num: this[ `${type.name}_num` ] } );
    this[ type.name ] = type;
    return (this[ `${type.name}_num` ] = Object.keys(this).length);
  },

  /**
   * Adds custom stringify method to the TypesEnum
   * @param {number} type number from the TypesEnum
   * @param {Function} Function number from the TypesEnum
   */
  addStringifier(type:number, func:Function):void {
    staticAssert( typeof Object.values( this ).find( i => i == type ) !== "undefined", "Given type must be in the TypesEnum", { number: type } );
    staticAssert( type > 7 , "You cannot assign custom stringify method for the primitive types.", { number: type, type: Object.keys(this).find( el => this[el] == type ) } );
    this[ `stringify_${type}` ] = func;
  },

  /**
   * Parses given variable of any type into the number that can be later used in TypesEnum
   * @param {any} type type to be parsedd
   * @returns {number} number that can be lated used in TypesEnum
   */
  getType(type:any):number {
    if( typeof type === "function" ) {
      if( typeof this[ type.name ] !== "undefined" ) return this[ `${type.name}_num` ]; // Custom user classes
      return this.function;
    }
    else if( Array.isArray( type ) ) return this.array;
    else if( typeof type === "object" ) {
      const key = Object.keys( this ).find( el => {
        try { return type instanceof this[el]; } catch(e) { return false; }
      } );
      return this[ key ? key + "_num" : "object" ]; // Custom user classes
    }
    else if( typeof type === "string" )    return this.string;
    else if( typeof type === "undefined" ) return this.undefined;
    else if( typeof type === "symbol" )    return this.symbol;
    else if( typeof type === "boolean" )   return this.boolean;
    else                                   return this.number; // Number like (number, bigint etc.)
  },

  function:  0,
  object:    1,
  string:    2,
  array:     3,
  undefined: 4,
  symbol:    5,
  boolean:   6,
  number:    7
};

/**
 * Pair implementation
 * @class
 * @template T1,T2
 */
export class Pair< T1, T2 > {

  /**
   * first element of the pair
   * @type {T1}
   * @public
   */
  public first:T1;

  /**
   * second element of the pair
   * @type {T2}
   * @public
   */
  public second:T2;

  /**
   * @param {T1} first member of the pair
   * @param {T2} second member of the pair
   * @public
   * @constructor
   */
  public constructor( first:T1, second:T2 ) {
    this.first = first;
    this.second = second;
  }

}

/**
 * Allows user to keep any type inside and additionally gives hashed index
 * @class
 * @template Type
 */
export class UniversalIndexHashHolder<Type> {
  /**
   * Data of any type and index to be used in binary trees or hashing
   * @type {Pair<Type,number>}
   * @private
   */
  private data:Pair< Type, number >;

  /**
   * @param {Type} data data of any type to be parsed into an index
   * @public
   * @constructor
   */
  public constructor( data:Type ) {
    const hasher:string = TypesEnum.stringify( data );
    this.data = new Pair<Type, number> ( data, hasher.hashCode() );

    staticAssert( typeof this.data.first === typeof data, "Generic and constructor argument types should be the same!\nTypes are: \'${t1}\' and \'${t2}\'", { t1: typeof this.data.first, t2: typeof data } );
  }

  /**
   * @returns {number} hashed index
   * @public
   */
  public getIndex():number {
    return this.data.second;
  }

  /**
   * @returns {Type} data
   * @public
   */
  public getData():Type {
    return this.data.first;
  }

}

// TODO: Queue
