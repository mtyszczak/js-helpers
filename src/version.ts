/**
 * version.ts - Version parser, compare, stringify and more
 *
 * @version 0.0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

// Local dependencies:
import { staticAssert } from "./assert";


/**
 * Contains version
 * @class
 */
export class Version {
  /**
   * Major version part
   * @type {number}
   * @private
   */
  private major:number;
  /**
   * Minor version part
   * @type {number}
   * @private
   */
  private minor:number;
  /**
   * Patch version part
   * @type {number}
   * @private
   */
  patch:number;

  /**
   * Constructs new Version with given version parts
   *
   * @param {number} major major version part (defaults to 0)
   * @param {number} minor minor version part (defaults to 0)
   * @param {number} patch patch version part (defaults to 0)
   * @throws {AssertionError} when major, minor or patch version part is less than 0
   * @public
   * @constructor
   */
  constructor( major:number = 0, minor:number = 0, patch:number = 0 ) {
    staticAssert( major >= 0 && minor >= 0 && patch >= 0, "Version cannot be less than 0" );
    this.major = major;
    this.minor = minor;
    this.patch = patch;
  }

  /**
   * Checks if current version is less than given one
   * @param {Version} other version to compare
   * @returns {boolean} true if current version is less than given one
   * @public
   */
  public less( other:Version ):boolean {
    if( this.major < other.major ) return true;
    if( this.minor < other.minor ) return true;
    if( this.patch < other.patch ) return true;
    return false;
  }

  /**
   * Checks if current version is greater than given one
   * @param {Version} other version to compare
   * @returns {boolean} true if current version is greater than given one
   * @public
   */
  public greater( other:Version ):boolean {
    if( this.major > other.major ) return true;
    if( this.minor > other.minor ) return true;
    if( this.patch > other.patch ) return true;
    return false;
  }

  /**
   * Checks if current version is same as given one
   * @param {Version} other version to compare
   * @returns {boolean} true if current version is same as given one
   * @public
   */
  public same( other:Version ):boolean {
    return this.major == other.major && this.minor == other.minor && this.patch == other.patch;
  }

  /**
   * Compares current version to the given one (works like a three way comparison operator)
   * @param {Version} other version to compare
   * @returns {number} positive value if current version is greater, negative when
   *                   less and 0 when the both versions are the same
   * @public
   */
  public compare( other:Version ):number {
    if( this.greater(other) ) return 1;
    else if( this.less(other) ) return -1;
    else return 0;
  }

  /**
   * Converts current object to the string format
   * @returns {string} string in x.y.z format
   * @public
   */
  public stringify():string {
    return `${this.major}.${this.minor}.${this.patch}`;
  }

  /**
   * Converts given object to the string format
   * @returns {string} string in x.y.z format
   * @public
   * @static
   */
  public static stringify( other:Version ):string {
    return `${other.major}.${other.minor}.${other.patch}`;
  }

  /**
   * Parses string into the current object
   * @param {string} other version string in x.y.z format
   * @public
   */
  public parse( other:string = "" ):void {
    const parts = other.split(".");
    this.major = ~~parts[0];
    this.minor = ~~parts[1];
    this.patch = ~~parts[2];
  }

  /**
   * Parses string into the new instance of the class
   * @param {string} other version string in x.y.z format
   * @returns {Version} brand new object from the parsed string
   * @public
   * @static
   */
  public static parse( other:string = "" ):Version {
    const parts = other.split(".");
    return new Version( ~~parts[0], ~~parts[1], ~~parts[2] );
  }
}

// TODO: Variant version